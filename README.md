# Software Resume

Nathan Jatau  
jataunathan@outlook.com  
 
Current Address: 	 
1250 Nottoway Trail 
Marietta, Georgia 30066 


Mobile: (770) 203-5714 
 
OBJECTIVE:  Seeking a career opportunity that will build on my 11 years of technical experience and direct training in software engineering for transitioning into a full-time software development role. 
	
               EDUCATION:  	University of Phoenix, Tampa, Arizona  
	 	                    Bachelor of Science in Advanced Software Development  
                            
 
   RELEVANT COURSES COMPLETED: 

•   Java programming, I, II, III-(Masters) 

•   C++ programming, I, II	                 •   (HTML+CSS) Web Development 

•  .Net framework VB I, C# II  	             •	Software Development Lifecycle 
 
             
			 
   EXPERIENCE: 
	              
   
   
   Randstad Staffing, Atlanta, GA                         February 2013-January 2018 


•	Maintained and programmed Warehouse Inventory for Oracle database systems 

•	Manually programmed the input UMO serial numbers in Microsoft Excel using scripts  

•	Programmed structures and permissions from a super user level within a Windows Telnet environment.

•	Administered SQL database queries alongside a windows terminal/ (command prompt).  


   
   
   Advance Auto Parts, Atlanta, GA 	                      October 2010 – December 2012 

•	Responsible for inventory over company merchandise 

•	Programmed Equipment for mobile RF scan terminals (EXT 250)   

•	Programmed Falcon operating file system for embedded devices.

•	File system hierarchy in supportive to pushing inventory release via Telnet OS queue from Windows Access Database



   
   
   Corporate Express, Atlanta, GA                         May 2007– August 2010 


•	Employed in a distributed warehouse facility  

•	Learned a programmable systems approach in warehousing logistics

•	Handled the inventory storage system in Telnet OS, Falcon OS, MS Excel scripts while configuring Network Drives  




TECHNICAL SKILLS: 


•	Oracle Inventory Database   

•	Microsoft Word, Excel, PowerPoint, Windows Telnet, SQL SRSS, SAS Analytical Programming–(Masters)

•	Java2EE Enterprise, Microsoft Visio, C++ Unreal, ISC2 Internet security 

•	Unix programming, Operating Systems, JavaScript, FPGA

•	Microsoft Access database entry (UPS, FedEx inventory system)  

•	Windows Embedded, POS Terminals, Service Com Ports, DHCP Network Ghost Protocols.

•	Testing registry (.bat) files with RU test applications.  

